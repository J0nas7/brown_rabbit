var Rows = "";
var Rowsize = 0;
var showRows = 3;
var StartIndex = 0;
var theElem = "";
var partOfRows = [];

window.addEventListener('load', function() {
  paginateIt(posts, "posts");
});

function paginateIt(theRows, elemClassName, newIndex = "", numRows = "") {
  theElem = document.getElementsByClassName(elemClassName)[0];
  Rows = theRows;
  Rowsize = Rows.length;

  if (-1 < newIndex && isNumeric(newIndex)) { StartIndex = newIndex; }
  if (numRows) { showRows = numRows; }
  printBody(StartIndex, parseInt(parseInt(StartIndex)+parseInt(showRows)));
}

function printBody(Start, End) {
  theElem.innerHTML = "";
  var thePosts = Rows.slice(Start, End);
  var postsHtml = "";
  for (i = 0; i < thePosts.length; i++) {
    var thisPost = thePosts[i];
    postsHtml = postsHtml +
                    '<article id="post1">'+
                      '<img src="img/'+thisPost.image+'.png" alt="'+thisPost.id+'" />'+
                      '<h3 class="searchAble">'+thisPost.title+'</h3>'+
                      '<span class="posted searchAble">'+thisPost.posted+'</span>'+
                      '<span class="thePost searchAble">'+thisPost.thePost+'</span>'+
                      '<span class="theReadMore searchAble">'+thisPost.theReadMore+'</span>'+
                      '<span class="readMore searchAble" onclick="readMore(event)">Read more</span>'+
                      '<div class="clrBth"></div>'+
                    '</article>';
  }
  postsHtml = postsHtml + '<div class="clrBth"></div>';
  theElem.innerHTML = "";
  if (theElem.innerHTML == "") {
    theElem.innerHTML = postsHtml;
  }

  if (Rowsize > showRows) {
    makePagination();
  }
}

function makePagination() {
  var numPages = (Rowsize/showRows);
  var Pagination = "<div class='Pagination-Wrapper'>";
  var DivPage = "<div class='Pagination' unselectable='on'>";

  for (i = 0; i < numPages; i++) {
    var makeIndex = (i*showRows);
    var pageNumber = (i+1);
    if (StartIndex == makeIndex) {
      var PageOf = "<span class='pageOf'>Page "+pageNumber+" of "+numPages+"</span>";
      DivPage = DivPage + PageOf;
    }
  }

  var PrevHtml = "&laquo;";
  if (0 == StartIndex) {
    var PrevClass = "abort";
  }
  var PrevElem = "<span class='prevPage "+PrevClass+"' onclick='prevNext(\"prev\")'>"+PrevHtml+"</span>";
  DivPage = DivPage + PrevElem;

  for (i = 0; i < numPages; i++) {
    var makeIndex = (i*showRows);
    var pageNumber = (i+1);
    var NavActive = "";
    if (StartIndex == makeIndex) {
      NavActive = "active";
    }

    var NavElem = "<span class='numeric "+NavActive+"' StartIndex='"+makeIndex+"' onclick='makeIndex("+makeIndex+")'>"+pageNumber+"</span>";
    DivPage = DivPage + NavElem;
  }

  var NextHtml = "&raquo;";
  if (Rows.length <= parseInt(parseInt(StartIndex)+parseInt(showRows))) {
    var NextClass = "abort";
  }
  var NextElem = "<span class='nextPage "+NextClass+"' onclick='prevNext(\"next\")'>"+NextHtml+"</span>";
  DivPage = DivPage + NextElem;

  Pagination = Pagination + DivPage + "</div>";
  Pagination = Pagination + "</div><div class='clrBth'></div>";
  theElem.innerHTML = theElem.innerHTML + Pagination;
  updateOpars();
}

function updateOpars() {
  opars = document.getElementsByClassName("searchAble");
  opar = [];
  for (i = 0; i < opars.length; i++) {
    opar[i] = opars[i].innerHTML;
  }
}

function makeIndex(newIndex) {
  var newIndex = parseInt(newIndex);
  paginateIt(Rows, "posts", newIndex);
}

function prevNext(type) {
  var prevNextElem;
  if (type == "prev") {
    var newIndex = parseInt(parseInt(StartIndex)-parseInt(showRows));
    prevNextElem = document.getElementsByClassName('prevPage')[0];
  } else if (type == "next") {
    var newIndex = parseInt(parseInt(StartIndex)+parseInt(showRows));
    prevNextElem = document.getElementsByClassName('nextPage')[0];
  }

  if (!prevNextElem.classList.contains("abort")) {
    paginateIt(Rows, "posts", newIndex);
  }
}

var posts = [
  {
    id: "post1",
    image: "post1",
    title: "Wonderful Copenhagen 2011",
    posted: "Posted: 23/1/2011",
    thePost: "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory...",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post2",
    image: "post2",
    title: "Nordic Barista Cup 2011 in Copenhagen",
    posted: "Posted: 22/1/2011",
    thePost: "Nordic Barista Cup 2011 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page....",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post3",
    image: "post3",
    title: "2010 Winners: Sweden",
    posted: "Posted: 12/1/2011",
    thePost: "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. ...",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post4",
    image: "post1",
    title: "Lorem Ipsum 4",
    posted: "Posted: 11/1/2011",
    thePost: "Yet another posts",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post5",
    image: "post2",
    title: "Lorem Ipsum 5",
    posted: "Posted: 10/1/2011",
    thePost: "Yet another posts2",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post6",
    image: "post3",
    title: "Lorem Ipsum 6",
    posted: "Posted: 9/1/2011",
    thePost: "Yet another posts3",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post7",
    image: "post1",
    title: "Lorem Ipsum 7",
    posted: "Posted: 8/1/2011",
    thePost: "Yet another posts4",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post8",
    image: "post1",
    title: "Lorem Ipsum 8",
    posted: "Posted: 7/1/2011",
    thePost: "Yet another posts4",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post9",
    image: "post2",
    title: "Lorem Ipsum 9",
    posted: "Posted: 8/1/2011",
    thePost: "Yet another posts5",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post10",
    image: "post3",
    title: "Lorem Ipsum 10",
    posted: "Posted: 7/1/2011",
    thePost: "Yet another posts6",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post11",
    image: "post1",
    title: "Lorem Ipsum 11",
    posted: "Posted: 6/1/2011",
    thePost: "Yet another posts7",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post12",
    image: "post2",
    title: "Lorem Ipsum 12",
    posted: "Posted: 5/1/2011",
    thePost: "Yet another posts6",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post13",
    image: "post3",
    title: "Lorem Ipsum 13",
    posted: "Posted: 4/1/2011",
    thePost: "Yet another posts7",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post14",
    image: "post1",
    title: "Lorem Ipsum 14",
    posted: "Posted: 3/1/2011",
    thePost: "Yet another posts8",
    theReadMore: "Lorem ipsum dolor sit amet"
  },
  {
    id: "post15",
    image: "post2",
    title: "Lorem Ipsum 15",
    posted: "Posted: 2/1/2011",
    thePost: "Yet another posts9",
    theReadMore: "Lorem ipsum dolor sit amet"
  }
];

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
